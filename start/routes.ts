/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'

// Public pages
Route.on('/').render('home');
Route.on('/about').render('about')
Route.on('/contact').render('contact')
Route.post('/contact', 'ContactsController.contact')

// Protected pages
Route.group(() =>
{

}).middleware('auth')

// Authentification pages
Route.group(() =>
{
    // sign-in
    Route.on('sign-in').render('sign-in')
    Route.post('sign-in', 'AuthController.signIn')

    // sign-up
    Route.on('sign-up').render('sign-up')
    Route.post('sign-up', 'AuthController.signUp')

    // sign-out
    Route.get('sign-out', 'AuthController.signOut').middleware('auth')
})

Route.on('*').render('errors/not-found')
