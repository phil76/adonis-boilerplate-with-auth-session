import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Contact from 'App/Models/Contact'

export default class ContactsController
{
    /**
     * Register a new user
     * @param form 
     */
    public async contact({ request, response }: HttpContextContract)
    {
        // Validate user details
        const validationSchema = schema.create({
            email: schema.string({ trim: true }, [
                rules.email(),
            ]),
            object: schema.string({ trim: true, escape: true }),
            message: schema.string({ trim: true, escape: true }),
        })

        const contactDetails = await request.validate({
            schema: validationSchema,
        })

        console.log(contactDetails);
        

        // Create the new contact
        const contact = new Contact()
        contact.email = contactDetails.email
        contact.object = contactDetails.object
        contact.message = contactDetails.message

        await contact.save()

        // Redirect to home
        response.redirect('/')
    }
}
