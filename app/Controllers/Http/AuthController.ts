import User from 'App/Models/User'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AuthController
{
    /**
     * Register a new user
     * @param form 
     */
    public async signUp({ request, auth, response }: HttpContextContract)
    {
        // Validate user details
        const validationSchema = schema.create({
            username: schema.string({ trim: true, escape: true }, [
                rules.unique({ table: 'users', column: 'username' }),
            ]),
            password: schema.string({ trim: true }, [
                rules.confirmed(),
            ])
        })

        const userDetails = await request.validate({
            schema: validationSchema,
        })

        // Create the new user
        const user = new User()
        user.username = userDetails.username
        user.password = userDetails.password

        await user.save()

        // Login the user
        await auth.login(user)

        // Redirect to home
        response.redirect('/')
    }

    /**
     * Login the user
     * @param form 
     */
    public async signIn({ auth, request, response }: HttpContextContract)
    {
        const username = request.input('username')

        const password = request.input('password')

        await auth.use('web').attempt(username, password)

        if (auth.isLoggedIn)
        {
            response.redirect('/')
        }
    }

    /**
     *  Logout the user
     */
    public async signOut({ auth, response, session }: HttpContextContract)
    {
        await auth.logout();

        session.flash({ notification: 'Logged out successfully' });

        return response.redirect('/');
    }
}
