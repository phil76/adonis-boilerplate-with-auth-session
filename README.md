# Adonis Boilerplate
https://docs.adonisjs.com/guides/context

Commande | Role
--- | --- 
`npm install` | install dependencies
`npm run dev` | start dev server

## Content
- authentification
- pureccs layout
- basic contact form

## Installation
```
git clone https://gitlab.com/phil76/adonis-boilerplate-with-auth-session.git
npm install
cp .env.example .env
node ace configure @adonisjs/lucid
node ace migration:run
npm run dev
```

## Debugger
Modifier cette ligne dans les scripts de package.json:

```"dev": "node ace serve --watch --node-args=\"--inspect\"",```

Dans vscode, cliquez sur debug, puis "create a launch.json file", choisissez Node.js.
Remplacer son contenu par:
```
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Debug Application",
            "runtimeExecutable": "npm",
            "runtimeArgs": [
                "run",
                "dev"
            ],
            "port": 9229,
            "internalConsoleOptions": "openOnSessionStart"
        }
    ]
}
```
Cliquez sur l'icone Play (icone vert)
Vous pouvez maintenant ajouter des breakpoint dans vos fichiers ts